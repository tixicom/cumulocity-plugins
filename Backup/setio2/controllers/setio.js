angular.module('tx.ioManager2')

	.controller('ioSetCtrl', ['$scope', '$routeParams', '$location', '$q', 'c8yBase', 'c8yDeviceControl', 'c8yModal', 'c8yAlert',
		function ($scope, $routeParams, $location, $q, c8yBase, c8yDeviceControl, c8yModal, c8yAlert) {
			'use strict';

			var openOperations = [],
				deviceStatus = {},
				latestCommandsCount = 3;

			function init() {
				initializeCommand();
				loadLatestCommands();
			}

			function initializeCommand() {
				$scope.precommand_b = '[<Set _="/Process/MB/IO/Q/P0" value="';
				$scope.precommand_e = '" ver="v"/>]';
				$scope.command = {text: ''};
				$scope.iostate = '';
			}

			function loadLatestCommands() {
				getLatestCommandOperations()
					.then(onOperations);
			}

			function setio() {
				$scope.command.text = $scope.precommand_b + io_state + $scope.precommand_e;
				execute($scope.command);
			}

			function getLatestCommandOperations() {
				var filters = {
					deviceId: $routeParams.deviceId,
					pageSize: 1,
					withTotalPages: true,
					fragmentType: 'c8y_Command'
				};

				return c8yDeviceControl.list(filters).then(function (list) {
					var commandPromises = [],
						totalPages = list.statistics.totalPages,
						pageCounter = 0;

					for (var page = totalPages; page > 1 && pageCounter < latestCommandsCount; page--, pageCounter++) {
						commandPromises.push(c8yDeviceControl.list(angular.extend(filters, {currentPage: page})));
					}

					return $q.all(commandPromises).then(function (results) {
						return _.flatten(results);
					});
				});
			}

			function onOperations(operations) {
				$scope.operations = operations;
			}

			/**
			 * @ngdoc function
			 * @name execute
			 * @methodOf c8y.deviceShell.controller:deviceShell
			 *
			 * @description
			 * Takes command object, checks if device is online
			 * and creates a new operation with realtime notifications enabled
			 * or displays error message.
			 *
			 * @param {object} command Command object to execute.
			 */
			function execute(command) {
				deviceStatus.refresh().then(function () {
					if (deviceStatus.isOnline()) {
						var operation = getOperation(command);
						c8yDeviceControl.createWithNotifications(operation).then(function (operationPromises) {
							operationPromises.created.then(_.partial(onOperationCreated, operation));
							operationPromises.completed.then(_.partial(onOperationUpdated, operation), angular.noop, _.partial(onOperationUpdated, operation));
						});
						command.text = '';
					} else {
						c8yAlert.danger('Could not send a command! Device is offline!');
					}
				});
			}

			function getOperation(command) {
				return {
					deviceId: $routeParams.deviceId,
					description: 'Execute io set: ' + command.name || command.text,
					c8y_Command: {
						text: command.text
					}
				};
			}

			function onOperationCreated(operation, operationId) {
				c8yDeviceControl.detail(operationId).then(c8yBase.getResData).then(function (operationResult) {
					c8yAlert.success('Command has been sent!');
					angular.extend(operation, operationResult);
					appendOperation(operation);
				});
			}

			function onOperationUpdated(operation, operationResult) {
				c8yAlert.success('Command\'s status has been updated!');
				angular.extend(operation, operationResult);
			}

			function appendOperation(operation) {
				$scope.operations.push(operation);
				collapseAllExpandOne(operation);
			}

			function collapseAllExpandOne(operation) {
				openOperations.length = 0;
				toggle(operation);
			}

			function toggle(operation) {
				if (isOpen(operation)) {
					var ix = openOperations.indexOf(operation);
					openOperations.splice(ix, 1);
				} else {
					openOperations.push(operation);
				}
			}

			function isOpen(operation) {
				return openOperations.indexOf(operation) > -1;
			}

			function statusClass(status) {
				return status && c8yDeviceControl.getStyle(status) && c8yDeviceControl.getStyle(status).cls;
			}

			function statusIcon(status) {
				return status && c8yDeviceControl.getStyle(status) && c8yDeviceControl.getStyle(status).icon;
			}

			function viewHistory() {
				$location.url(getViewHistoryLink());
			}

			function getViewHistoryLink() {
				return '/device/' + $routeParams.deviceId + '/control?fragmentType=c8y_Command';
			}

			$scope.setio = setio;
			$scope.execute = execute;
			$scope.toggle = toggle;
			$scope.isOpen = isOpen;
			$scope.statusClass = statusClass;
			$scope.statusIcon = statusIcon;
			$scope.cancel = c8yDeviceControl.cancel;
			$scope.viewHistory = viewHistory;
			$scope.getViewHistoryLink = getViewHistoryLink;
			$scope.deviceStatus = deviceStatus;

			init();
		}
	]);
