angular.module('tx.ioManager2')

/**
 * @ngdoc directive
 * @name c8y.deviceShell.directive:deviceStatus
 *
 * @description
 * Directive displays current device status as well as allows for refreshing it.
 */
.directive('deviceStatus2', function () {
  'use strict';
  
  return {
    restrict: 'E',
    templateUrl: ':::PLUGIN_PATH:::/views/deviceStatus.html',
    controller: 'deviceStatusCtrl2',
    scope: {
      device: '=',
      isOnline: '=',
      refresh: '='
    }
  };
});
