angular.module('tx.mbusScanner', [])
	.config(['c8yViewsProvider', function (c8yViewsProvider) {
		var icon = 'gamepad',
			name = 'M-BUS Scanner',
			path = '/device/:deviceId',
			templatePath = ':::PLUGIN_PATH:::/views/';

		// Add tab to device details view:
		c8yViewsProvider.when(path, {
			name: name,
			icon: icon,
			templateUrl: templatePath + 'index.html',
			// show this tab only if device supports c8y_Command operation:
			showIf: ['$routeParams', 'c8yBase', 'c8yDevices', function ($routeParams, c8yBase, c8yDevices) {
				return c8yDevices.detailCached($routeParams.deviceId).then(c8yBase.getResData).then(function (device) {
					return c8yDevices.supportsOperation(device, 'c8y_Command');
				});
			}]
		});
	}]);
