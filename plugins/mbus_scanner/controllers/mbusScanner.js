angular.module('tx.mbusScanner')
	.controller('mbusScannerCtrl', ['deviceStatus', '$scope', '$routeParams', '$location', '$q', 'c8yBase', 'c8yDeviceControl', 'c8yModal', 'c8yAlert',
		function (deviceStatus, $scope, $routeParams, $location, $q, c8yBase, c8yDeviceControl, c8yModal, c8yAlert) {
			'use strict';

			function init() {
                initializeModel();
			}

			function initializeModel() {
				$scope.devices = [];
			}

			function scanMbus() {
				sendCommand("M-Bus Scan", '[<ScanDevices _="COM3" protocol="meterbus" ver="v"/>]', parseScanResult);
			}

			function sendCommand(name, cmd, callback) {
				var command = {
					text: cmd,
					name: name
				};

				deviceStatus.refresh().then(function () {
                    if (deviceStatus.isOnline()) {
                        var operation = getOperation(command);
                        c8yDeviceControl.createWithNotifications(operation).then(function (operationPromises) {
                            operationPromises.created.then(_.partial(onOperationCreated, operation));
                            operationPromises.completed.then(_.partial(callback, operation), angular.noop, _.partial(callback, operation));
                        });
                    } else {
                        c8yAlert.danger('Could not send a command! Device is offline!');
                    }
                });
			}

            function parseScanResult(operation, operationResult) {
                onOperationUpdated(operation, operationResult);
                if (operationResult.status == "SUCCESSFUL") {
                    var result = operationResult.c8y_Command.result;

					// $scope
                    var scanResult = result.split(/\r\n/g); // string to array
                    scanResult.pop(); // Remove ScanDevices start tag
                    scanResult.shift(); // Remove ScanDevices end tag

                    var re_result;
                    var re_device = /^<Device PrimaryAddr="([0-9]+)" SecondaryAddr="([0-9]+)" Manufacturer="(.+)" Version="([0-9]*)" Medium="(.+)" State="(.+)">$/;
                    var re_device_var = /^<Var[0-9]+ ind="(.+)" vib="(.*)" value="(.*)" \/>$/;

                    for (var i = 0; i < scanResult.length; i++) {
                        // Detect device
                        re_result = re_device.exec(scanResult[i]);
                        if (re_result) {
							var device = {
								primaryAddr: re_result[1],
								secondaryAddr: re_result[2],
								manufacturer: re_result[3],
								version: re_result[4],
								medium: re_result[5],
								state: re_result[6],
								vars: []
							};

							devices.push(device);

                            continue;
                        }

                        // Detect element
                        re_result = re_device_var.exec(scanResult[i]);
                        if (re_result) {
							var d_var = {
								ind: re_result[2],
								vib: re_result[3],
								value: re_result[4]
							};

							device[device.length].vars.push(d_var);
                        }
                    }

                    $scope.isLoadingValues = false;
                }
            }


			function getOperation(command) {
				return {
					deviceId: $routeParams.deviceId,
					description: 'Execute command: ' + command.name || command.text,
					c8y_Command: {
						text: command.text
					}
				};
			}

			function onOperationCreated(operation, operationId) {
				c8yDeviceControl.detail(operationId).then(c8yBase.getResData).then(function (operationResult) {
					c8yAlert.success('Command has been sent!');
				});
			}

			function onOperationUpdated(operation, operationResult) {
				c8yAlert.success('Command\'s status has been updated!');
			}

			$scope.cancel = c8yDeviceControl.cancel;
			$scope.deviceStatus = deviceStatus;
			$scope.scanMbus = scanMbus;

			init();
		}
	]);
