angular.module('tx.ioManager')
	.controller('ioManagerCtrl', ['deviceStatus', '$scope', '$routeParams', '$location', '$q', 'c8yBase', 'c8yDeviceControl', 'c8yModal', 'c8yAlert',
		function (deviceStatus, $scope, $routeParams, $location, $q, c8yBase, c8yDeviceControl, c8yModal, c8yAlert) {
			'use strict';

			var supportedIoModules = [],
				modulesData = [],
                settingOutputId = null,
                settingOutputValue = null;

			function init() {
                initializeModel();
                loadSupportedModules();
                getModules();
			}

			function initializeModel() {
				$scope.ioModules = [];
				$scope.blockInput = false;
			}

			function addSupportedModule(type, model, name, range, path) {
				var moduleType = {
					1: "Internal-Out",
					2: "External-Out",
                    3: "Internal-In",
                    4: "Internal-Out"
				};

				var module = { type: moduleType[type], model: model, name: name, range: range, path: path, addr: '' };

				supportedIoModules.push(module);
			}

			function loadSupportedModules() {
                // Format: type, model, name, range, path
                // type 1 = Internal
                // type 2 = External

				// Internal
				addSupportedModule(1, "GP21D-I/O", "Output", "0-1", "/MB/IO/Q/");
				addSupportedModule(1, "GP22D-I/O", "Output", "0-1", "/MB/IO/Q/");
				addSupportedModule(1, "GP23D-I/O", "Output", "0-1", "/MB/IO/Q/");
				addSupportedModule(1, "GP44D-I/O", "Output", "0-1", "/MB/IO/Q/");
				addSupportedModule(1, "GP82D-I/O", "Output", "0-1", "/MB/IO/Q/");
				addSupportedModule(1, "GP101D-I/O", "Output", "0-1", "/MB/IO/Q/");

				// External
				addSupportedModule(2, "XP84D-I/O", "Output", "0-1", "/Q/");
				addSupportedModule(2, "XP84DR-I/O", "Output", "0-1", "/Q/");
				addSupportedModule(2, "XP88D-I/O", "Output", "0-1", "/Q/");
				addSupportedModule(2, "XP88DR-I/O", "Output", "0-1", "/Q/");
				addSupportedModule(2, "S1-D05G-I/O", "Output", "0-1", "/Q/");
				addSupportedModule(2, "S1-D03G-I/O", "Output", "0-1", "/Q/");
				addSupportedModule(2, "S1-WL2", "Output", "0-1", "/Q/");
				addSupportedModule(2, "DAC 1*12bit", "Output", "0-4095", "/AO/");
                // Internal In
                addSupportedModule(3, "GP22D-I/O", "Input", "0-1", "/MB/IO/I/");
			}
			function checkModule(model) {
				for (var i = 0; i < supportedIoModules.length; i++) {
					if (supportedIoModules[i].hasOwnProperty('model') && supportedIoModules[i].model == model)
                        return supportedIoModules[i];
				}

				return null;
			}

			function addModule(addr, model){
                var module = angular.copy(checkModule(model));
				if (module) {
					module.addr = addr;
                    module.outputs = [];

                    switch(module.type) {
                        case "Internal":
                            module.path = "/Process" + module.path;
                            break;
                        case "External":
                            module.path = "/Process/" + addr + module.path;
                            break;
                        default:
                            break;
                    }

                    $scope.ioModules.push(module);
				}
			}

			function getModules() {
                $scope.isLoadingModules = true;

				var command = {text: '', name: ''};
				command.name = 'getModulesList';
				command.text = '[<Get _="/Hardware/Modules/" ver="v"/>]';
                deviceStatus.refresh().then(function () {
                    if (deviceStatus.isOnline()) {
                        var operation = getOperation(command);
                        c8yDeviceControl.createWithNotifications(operation).then(function (operationPromises) {
                            operationPromises.created.then(_.partial(onOperationCreated, operation));
                            operationPromises.completed.then(_.partial(parseModules, operation), angular.noop, _.partial(parseModules, operation));
                        });
                    } else {
                        c8yAlert.danger('Could not send a command! Device is offline!');
                    }
                });
			}

			function parseModules(operation, operationResult) {
				onOperationUpdated(operation, operationResult);
                if (operationResult.status == "SUCCESSFUL") {
                    var result = operationResult.c8y_Command.result;

                    modulesData = result.split(/\r\n/g); // string to array
                    modulesData.pop(); // remove Get
                    modulesData.shift(); // remove Get

                    var re_pattern = /<(.*) _="(.*)"\/>/;

                    for (var i = 0; i < modulesData.length; i++) {
                        var re_result = re_pattern.exec(modulesData[i]);
                        if (re_result) {
                            var addr = re_result[1];
                            var model = re_result[2];

                            addModule(addr, model);
                        }
                    }

                    $scope.isLoadingModules = false;

                    getModulesData(); // Now get the outputs and their state
                }
			}

            function getModulesData() {
                $scope.isLoadingValues = true;

                var command = {text: '', name: ''};
                command.name = 'getModulesList';
                command.text = '[<Get _="/Process/" ver="v"/>]';
                deviceStatus.refresh().then(function () {
                    if (deviceStatus.isOnline()) {
                        var operation = getOperation(command);
                        c8yDeviceControl.createWithNotifications(operation).then(function (operationPromises) {
                            operationPromises.created.then(_.partial(onOperationCreated, operation));
                            operationPromises.completed.then(_.partial(parseModulesData, operation), angular.noop, _.partial(parseModulesData, operation));
                        });
                    } else {
                        c8yAlert.danger('Could not send a command! Device is offline!');
                    }
                });
            }

            function parseModulesData(operation, operationResult) {
                onOperationUpdated(operation, operationResult);
                if (operationResult.status == "SUCCESSFUL") {
                    var result = operationResult.c8y_Command.result;

                    modulesData = result.split(/\r\n/g); // string to array
                    modulesData.pop(); // remove Get start tag
                    modulesData.shift(); // remove Get end tag

                    var currentPath = '';
                    var re_result;
                    var re_pattern_startTag = /^<([^\/]+)>$/;
                    var re_pattern_endTag  = /^<\/(.*)>$/;
                    var re_pattern_element = /^<(.*) _="(.*)"\/>$/;

                    for (var i = 0; i < modulesData.length; i++) {
                        // Detect start tag
                        re_result = re_pattern_startTag.exec(modulesData[i]);
                        if (re_result) {
                            currentPath += '/' + re_result[1];

                            continue;
                        }

                        // Detect element
                        re_result = re_pattern_element.exec(modulesData[i]);
                        if (re_result) {
                            var e_name = re_result[1];
                            var e_value = re_result[2];

                            addModuleData(e_name, e_value, currentPath);

                            continue;
                        }

                        // Detect end tag
                        re_result = re_pattern_endTag.exec(modulesData[i]);
                        if (re_result) {
                            var re_pattern_remove = new RegExp('/'+re_result[1]+'$');
                            currentPath = currentPath.replace(re_pattern_remove, '');
                        }
                    }

                    $scope.isLoadingValues = false;
                }
            }

            function addModuleData(name, value, path) {
                for (var i = 0; i < $scope.ioModules.length; i++) {
                    if ($scope.ioModules[i].path == path + '/') {
                        var output = {
                            id: $scope.ioModules[i].outputs.length + i*10 + 1,
                            name: name,
                            value: value,
                            isSettingOutput: false
                        };
                        $scope.ioModules[i].outputs.push(output);

                        break;
                    }
                }
            }

            function updateOutputByOutput(output) {
                for (var i = 0; i < $scope.ioModules.length; i++) {
                    for (var i2 = 0; i2 < $scope.ioModules[i].outputs.length; i2++) {
                        if ($scope.ioModules[i].outputs[i2].id == output.id) {
                            $scope.ioModules[i].outputs[i2].name = output.name;
                            $scope.ioModules[i].outputs[i2].value = output.value;
                            $scope.ioModules[i].outputs[i2].isSettingOutput = output.isSettingOutput;
                        }
                    }
                }
            }

            function updateOutputById() {
                for (var i = 0; i < $scope.ioModules.length; i++) {
                    for (var i2 = 0; i2 < $scope.ioModules[i].outputs.length; i2++) {
                        if ($scope.ioModules[i].outputs[i2].id == settingOutputId) {
                            $scope.ioModules[i].outputs[i2].value = settingOutputValue;
                        }
                    }
                }
            }

            function disableSettingOutputById(id) {
                for (var i = 0; i < $scope.ioModules.length; i++) {
                    for (var i2 = 0; i2 < $scope.ioModules[i].outputs.length; i2++) {
                        if ($scope.ioModules[i].outputs[i2].id == id) {
                            $scope.ioModules[i].outputs[i2].isSettingOutput = false;
                        }
                    }
                }
            }

            function setOutput(module, output) {
                $scope.blockInput = true;
                output.isSettingOutput = true;
                settingOutputId = output.id;
                settingOutputValue = output.value;

                var command = {text: '', name: ''};
                command.name = 'setOutput';
                command.text = '[<Set _="'+ module.path + output.name +'" value="'+ output.value +'" ver="v"/>]';

                if (deviceStatus.isOnline()) {
                    var operation = getOperation(command);
                    c8yDeviceControl.createWithNotifications(operation).then(function (operationPromises) {
                        operationPromises.created.then(_.partial(onOperationCreated, operation));
                        operationPromises.completed.then(_.partial(setOutputResponse, operation), angular.noop, _.partial(setOutputResponse, operation));
                    });
                } else {
                    c8yAlert.danger('Could not send a command! Device is offline!');
                }
            }

            function setOutput(module, output, value) {
                $scope.blockInput = true;
                output.isSettingOutput = true;
                settingOutputId = output.id;
                console.log(output);
                settingOutputValue = value;

                var command = {text: '', name: ''};
                command.name = 'setOutput';
                command.text = '[<Set _="'+ module.path + output.name +'" value="'+ value +'" ver="v"/>]';

                if (deviceStatus.isOnline()) {
                    var operation = getOperation(command);
                    c8yDeviceControl.createWithNotifications(operation).then(function (operationPromises) {
                        operationPromises.created.then(_.partial(onOperationCreated, operation));
                        operationPromises.completed.then(_.partial(setOutputResponse, operation), angular.noop, _.partial(setOutputResponse, operation));
                    });
                } else {
                    c8yAlert.danger('Could not send a command! Device is offline!');
                }
            }

            function setOutputResponse(operation, operationResult) {
				onOperationUpdated(operation, operationResult);

                if (operationResult.status == "SUCCESSFUL") {
                    $scope.blockInput = false;

					disableSettingOutputById(settingOutputId);
                    updateOutputById();
                } else if (operationResult.status =="FAILED") {
                    c8yAlert.danger('Something went wrong, bad command?');
                    $scope.blockInput = false;
                    settingOutputValue = null;

					disableSettingOutputById(settingOutputId);
                }

            }

			function getOperation(command) {
				return {
					deviceId: $routeParams.deviceId,
					description: 'Execute command: ' + command.name || command.text,
					c8y_Command: {
						text: command.text
					}
				};
			}

			function onOperationCreated(operation, operationId) {
				c8yDeviceControl.detail(operationId).then(c8yBase.getResData).then(function (operationResult) {
					c8yAlert.success('Command '+ operationResult.name +' has been sent!');
				});
			}

			function reload() {
				if ($scope.isSettingOutput) {
					$scope.isSettingOutput = false;
					c8yAlert.danger('Something went wrong.. Refreshing!');
					init();
				}
			}

			function onOperationUpdated(operation, operationResult) {
				c8yAlert.success('Command\'s status has been updated!');
			}

			$scope.cancel = c8yDeviceControl.cancel;
			$scope.deviceStatus = deviceStatus;
            $scope.setOutput = setOutput;

			init();
		}
	]);
