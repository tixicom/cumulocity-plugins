angular.module('tx.ioManager')

.controller('deviceStatusCtrl', ['$scope', '$routeParams', 'c8yBase', 'c8yDevices',
  function ($scope, $routeParams, c8yBase, c8yDevices) {
    'use strict';
    
    var deviceOnlineStatuses = ['CONNECTED', 'AVAILABLE'];
    
    function refresh() {
      $scope.refreshing = true;
      return c8yDevices.detail($routeParams.deviceId)
        .then(c8yBase.getResData)
        .then(onDevice)
        .finally(function () {
          $scope.refreshing = false;
        });
    }
    
    function onDevice(device) {
      $scope.device = device;
    }
    
    function isOnline() {
      return _.contains(deviceOnlineStatuses, c8yDevices.parseAvailability($scope.device));
    }
    
    function getStatus() {
      if (isOnline($scope.device)) {
        return 'Device is online';
      } else {
        return 'Device is offline';
      }
    }
    
    $scope.refresh = refresh;
    $scope.getStatus = getStatus;
    $scope.isOnline = isOnline;
  }
]);
