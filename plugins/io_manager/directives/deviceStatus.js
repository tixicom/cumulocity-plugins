angular.module('tx.ioManager')

/**
 * @ngdoc directive
 * @name c8y.deviceShell.directive:deviceStatus
 *
 * @description
 * Directive displays current device status as well as allows for refreshing it.
 */
.directive('tx-deviceStatus', function () {
  'use strict';
  
  return {
    restrict: 'E',
    templateUrl: ':::PLUGIN_PATH:::/views/deviceStatus.html',
    controller: 'deviceStatusCtrl',
    scope: {
      device: '=',
      isOnline: '=',
      refresh: '='
    }
  };
});
