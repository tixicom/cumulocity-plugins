angular.module('tx.ioManager')
	.factory('deviceStatus', ['$routeParams', 'c8yBase', 'c8yDevices',
  function ($routeParams, c8yBase, c8yDevices) {
    'use strict';

    var deviceOnlineStatuses = ['CONNECTED', 'AVAILABLE'],
		device = {};

    function onDevice(device_b) {
        device = device_b;
    }

    return {
      refresh: function() {
        return c8yDevices.detail($routeParams.deviceId)
          .then(c8yBase.getResData)
          .then(onDevice)
          .finally(function () {
          });
      },
      isOnline: function() {
        return _.contains(deviceOnlineStatuses, c8yDevices.parseAvailability(device));
      },
      getStatus: function() {
        if (this.isOnline(device)) {
          return 'Device is online';
        } else {
          return 'Device is offline';
        }
      }
    };
  }
]);
